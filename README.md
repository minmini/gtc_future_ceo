# GTC2020  Repo for Intructor-led training for CEOs for Future CEOs

This repo contains files needed to build a docker container that willl be used in the GTC2020 instructor-led training titled, "CEOs and Future CEOs: start Here for Hands-on AI".

# Docker Instructions

Use `Dockerfile` to build a docker image with `docker build -t gtc_future_ceo .`.

Run a container from this image with `docker run --gpus all --rm -it -p 8888:8888 gtc_future_ceo`.

Once the container is running, visit the content in your browser at `localhost:8888`.
