{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![header](salaries.png)\n",
    "# Real-World Linear Regression\n",
    "\n",
    "In this notebook, we  encounter a real-world situation where we can implement the linear regression model. Let us take the example of a company where there is a salary distribution based on years of experience and interview test score. We would like to create a model to predict the salary that HR would offer to a potential employee based on both their years of experience and interview assessment score.\n",
    "\n",
    "In this notebook, we will:\n",
    "- Divide the data into test and validation sets\n",
    "- Employ a Stochastic Gradient Descent model to fit the data\n",
    "- Evaluate our model against validation data\n",
    "- Use a regularized linear regression model to find a better fit\n",
    "- Modify hyperparameters to further improve our model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Import Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us start by importing our data.\n",
    "\n",
    "General rule of thumb: We want\n",
    "1. Lots of data\n",
    "2. Clean Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "# Importing the dataset\n",
    "dataset = pd.read_csv('salary_data.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset.describe()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "dataset.hist(bins=50, figsize=(20,15))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training, Validation and Test Set\n",
    "\n",
    " - **Training Set:** Data that will be used for creating a model using a set of parameters.\n",
    " - **Validation Set:** Subset of training set to measure how the training set performs with a set of parameters, and to compare different models.\n",
    " - **Test Set:** Set to measure how the model will perform in real life.\n",
    " \n",
    " ![alt-text](http://minminiventures.com/future_ceo/training_validation_testing.png)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "# Randomly split data into training, validation and test sets\n",
    "train_set_final, testing_set = train_test_split(dataset, test_size = 0.2, random_state=95)\n",
    "train_set, validation_set = train_test_split(train_set_final, test_size=0.3, random_state=1)\n",
    "\n",
    "# X will be a matrix of features (years experience and test score)\n",
    "# y will be an array of labels that we will measure our predictions agains (salaries)\n",
    "\n",
    "# Initial Training Set\n",
    "training_y = train_set[\"Salary\"].copy() \n",
    "training_X = train_set.drop(\"Salary\", axis=1) # columns except for Salary\n",
    "\n",
    "# Validation data to set results from various hyperparameters\n",
    "validation_y = validation_set[\"Salary\"].copy()\n",
    "validation_X = validation_set.drop(\"Salary\", axis=1)# columns except for Salary\n",
    "\n",
    "# Data to be used to train the final model\n",
    "training_final_y = train_set_final[\"Salary\"].copy()\n",
    "training_final_X = train_set_final.drop(\"Salary\", axis=1) # columns except for Salary\n",
    "\n",
    "# Data to test the final model\n",
    "testing_y = testing_set[\"Salary\"].copy()\n",
    "testing_X = testing_set.drop(\"Salary\", axis=1)# columns except for Salary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_set.plot(kind=\"scatter\", x=\"Salary\", y=\"YearsExperience\", alpha=0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_set.plot(kind=\"scatter\", x=\"Salary\", y=\"Interview Score\", alpha=0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_set.plot(kind=\"scatter\", x=\"YearsExperience\", y=\"Interview Score\", alpha=0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Gradient Descent & Stochastic Gradient Descent\n",
    "![alt-text](https://upload.wikimedia.org/wikipedia/commons/6/68/Gradient_descent.jpg)\n",
    "![alt-text](https://upload.wikimedia.org/wikipedia/commons/a/a3/Gradient_descent.gif)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us use the stochastic Gradient Descent function to find a linear fit for the training data. Gradient descent is a generic optimization algorithm capable of finding an optimal model by tweaking parameters iteratively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import SGDRegressor\n",
    "from sklearn.metrics import mean_squared_error\n",
    "\n",
    "# Training the model using Stochastic Gradient Descent\n",
    "sgd_reg = SGDRegressor(max_iter=100000, tol=1e-5, penalty=None, eta0=0.00001)\n",
    "sgd_reg.fit(training_X, training_y)\n",
    "\n",
    "# Predict the salaries for the training data\n",
    "sgd_predict_train = sgd_reg.predict(training_X);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After training and making a prediction with our model, let's evaluating the accuracy of our model against the training labels.\n",
    "\n",
    "One possible metric to use to evaluate the model is the Root Mean Square Error. This metric provides a measure of how accurate our predictions are against actual data labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "# Calculating the Mean Squared Error of the prediction\n",
    "lin_mse = mean_squared_error(training_y, sgd_predict_train)\n",
    "lin_rmse = np.sqrt(lin_mse)\n",
    "print(\"rmse= \", lin_rmse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us also validate the model against our validation set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Predicting the Validation set results\n",
    "sgd_preds_validation = sgd_reg.predict(validation_X)\n",
    "\n",
    "# Calculating the Mean Squared Error of the prediction\n",
    "lin_mse = mean_squared_error(validation_y, sgd_preds_validation)\n",
    "lin_rmse = np.sqrt(lin_mse)\n",
    "print(\"rmse= \", lin_rmse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the model performs poorly when applied against the validation data is. This is an indication that the model is overfitting the training data and does not perform well when exposed to new data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Regularization and Hyperparameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Regularization is a technique to constrain a model to make it simpler and reduce the risk of overfitting. Typically, constraints are added to the model's parameters. In linear models, the model parameters are the slope and y-intercept.\n",
    "\n",
    "We are going to train our model using Ridge Regression function, which is a regularized version of Linear Regression. The function uses the hyperparameter \"alpha\". \n",
    "\n",
    "If alpha = 0, then the Ridge Regressions is just Linear Regression. If alpha is very large, then all weights end up close to zero and the result is a flat line going through the data's mean. \n",
    "\n",
    "In this example, we will train three different models using different values of alpha."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import Ridge\n",
    "\n",
    "#Running a regularized linear model with various tunings of the parameter alpha\n",
    "ridge_reg = Ridge(alpha=0.1, solver=\"cholesky\")\n",
    "ridge_reg.fit(training_X, training_y)\n",
    "ridge_predict_train = ridge_reg.predict(training_X);\n",
    "\n",
    "ridge_reg_10 = Ridge(alpha=10, solver=\"cholesky\")\n",
    "ridge_reg_10.fit(training_X, training_y)\n",
    "ridge_predict_10_train = ridge_reg_10.predict(training_X);\n",
    "\n",
    "ridge_reg_100 = Ridge(alpha=100, solver=\"cholesky\")\n",
    "ridge_reg_100.fit(training_X, training_y)\n",
    "ridge_predict_100_train = ridge_reg_100.predict(training_X);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Validation\n",
    "We want to evaluate these three models with data that the model has not trained against. Therefore, we will fit the models with our validation data and compare the RMSE for each value of the hyperparamter alpha."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Predicting the Validation set results\n",
    "ridge_predict_validation = ridge_reg.predict(validation_X);\n",
    "ridge_predict_10_validation = ridge_reg_10.predict(validation_X);\n",
    "ridge_predict_100_validation = ridge_reg_100.predict(validation_X);\n",
    "\n",
    "\n",
    "# Calculating the Mean Squared Error of the prediction\n",
    "lin_mse = mean_squared_error(validation_y, ridge_predict_validation)\n",
    "lin_rmse = np.sqrt(lin_mse)\n",
    "print(\"alpha = 0.1: rmse= \", lin_rmse)\n",
    "\n",
    "lin_mse = mean_squared_error(validation_y, ridge_predict_10_validation)\n",
    "lin_rmse = np.sqrt(lin_mse)\n",
    "print(\"alpha = 10: rmse= \", lin_rmse)\n",
    "\n",
    "lin_mse = mean_squared_error(validation_y, ridge_predict_100_validation)\n",
    "lin_rmse = np.sqrt(lin_mse)\n",
    "print(\"alpha = 100: rmse= \", lin_rmse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the regularized model fits best with alpha hyperparaemter set to 10. So let us train the final model using our entire training set (including the validation subset) with alpha = 10."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Final model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ridge_reg_10 = Ridge(alpha=10, solver=\"cholesky\")\n",
    "ridge_reg_10.fit(training_final_X, training_final_y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have our final model, we can evaluate it against our test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ridge_predict_10_validation = ridge_reg_10.predict(testing_X);\n",
    "\n",
    "lin_mse = mean_squared_error(testing_y, ridge_predict_10_validation)\n",
    "lin_rmse = np.sqrt(lin_mse)\n",
    "print(\"alpha = 10: rmse= \", lin_rmse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Non-Linear Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may often encounter data that initially appears linear, but is non-linear in its entirety. For example, the employee salary data is linear up to 10 years. However, when including data of employees with 10-30 years of experience, we see that the linear model no longer can fit the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Importing the dataset\n",
    "dataset = pd.read_csv('salary_data_more_experience.csv')\n",
    "X_enhanced = dataset.iloc[:, :-1].values #get a copy of dataset exclude last column\n",
    "y_enhanced = dataset.iloc[:, 1].values #get array of dataset in column 1st\n",
    "\n",
    "#split into testing and training data\n",
    "X_train_enhanced, X_test_enhanced, y_train_enhanced, y_test_enhanced = train_test_split(X_enhanced, y_enhanced, test_size=1/3, random_state=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualizing the results\n",
    "viz_test = plt\n",
    "viz_test.scatter(X_train_enhanced, y_train_enhanced, color='red')\n",
    "viz_test.title('Salary VS Experience (Train set)')\n",
    "viz_test.xlabel('Year of Experience')\n",
    "viz_test.ylabel('Salary')\n",
    "viz_test.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Non-Linear Modeling - Random Forest Regressor\n",
    "The Random Forest model is a non-linear model that we will use to fit this non-linear data.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.ensemble import RandomForestRegressor\n",
    "forest_reg = RandomForestRegressor()\n",
    "forest_reg.fit(X_train_enhanced, y_train_enhanced)\n",
    "y_predictions = forest_reg.predict(X_train_enhanced)\n",
    "\n",
    "viz_test = plt\n",
    "viz_test.scatter(X_train_enhanced, y_train_enhanced, color='red', marker='x', s=50, label='Label')\n",
    "viz_test.scatter(X_train_enhanced, y_predictions, color='blue', s=15, label='Prediction')\n",
    "viz_test.title('Salary VS Experience (Train set) Non Linear Model')\n",
    "viz_test.xlabel('Year of Experience')\n",
    "viz_test.ylabel('Salary')\n",
    "viz_test.show()\n",
    "\n",
    "forest_mse = mean_squared_error(y_train_enhanced, y_predictions)\n",
    "forest_rmse = np.sqrt(forest_mse)\n",
    "print(\"rmse= \", forest_rmse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Multiple Features\n",
    "Years of experience may not be the only feature that impacts salary. In the following example, we train data with multiple features such as location, gender, interview score and education, to model salary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Importing the dataset with many features\n",
    "dataset = pd.read_csv('salary_data_more_experience_multi_params.csv')\n",
    "train_set, test_set = train_test_split(dataset, test_size = 0.2, random_state = 43)\n",
    "\n",
    "# View first five rows of data\n",
    "train_set.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## One Hot Encoding\n",
    " ![alt-text](http://minminiventures.com/future_ceo/onehotencoding.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.compose import ColumnTransformer\n",
    "from sklearn.preprocessing import OneHotEncoder\n",
    "from sklearn.pipeline import Pipeline\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "from sklearn.impute import SimpleImputer\n",
    "\n",
    "# identify labels and features\n",
    "salary_labels = train_set[\"Salary\"].copy()\n",
    "salary_features = train_set.drop(\"Salary\", axis=1)\n",
    "\n",
    "# identify features with numeric values\n",
    "salary_num = salary_features.drop(\"Gender\", axis=1).drop(\"Location\", axis=1).drop(\"Education\", axis=1)\n",
    "num_attribs = list(salary_num)\n",
    "\n",
    "# create a pipeline to scale the numeric values so that they are all are within a similar range. \n",
    "# This type of scaling can speed up computation\n",
    "num_pipeline = Pipeline([\n",
    "    ('imputer', SimpleImputer(strategy=\"median\")),\n",
    "    ('std_scaler', StandardScaler()),\n",
    "])\n",
    "\n",
    "# Identify non-numerical features that need to be converted into numerical values before the modeling can be done\n",
    "gender_attribs = [\"Gender\"]\n",
    "location_attribs = [\"Location\"]\n",
    "education_attribs = [\"Education\"]\n",
    "\n",
    "# Create a pipeline that will create scaled, numeric data for all the features\n",
    "full_pipeline = ColumnTransformer([\n",
    "    (\"num\", num_pipeline, num_attribs),\n",
    "    (\"edu\", OneHotEncoder(), education_attribs),\n",
    "    (\"gen\", OneHotEncoder(), gender_attribs),\n",
    "    (\"loc\", OneHotEncoder(), location_attribs)\n",
    "    ])\n",
    "\n",
    "# Generate the prepared data by passing it through the pipeline\n",
    "salaries_prepared = full_pipeline.fit_transform(salary_features)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This prepared multivariable and non-linear data can be modeled using the Random Forest model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Train and Predict Salaries\n",
    "forest_reg = RandomForestRegressor()\n",
    "forest_reg.fit(salaries_prepared, salary_labels)\n",
    "y_predictions = forest_reg.predict(salaries_prepared)\n",
    "\n",
    "# Calculate the RMSE\n",
    "forest_mse = mean_squared_error(salary_labels, y_predictions)\n",
    "forest_rmse = np.sqrt(forest_mse)\n",
    "print(\"rmse= \", forest_rmse)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
